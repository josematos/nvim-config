vim.cmd("set expandtab")
vim.cmd("set tabstop=4")
vim.cmd("set softtabstop=4")
vim.cmd("set shiftwidth=4")
vim.cmd("set number")
vim.g.mapleader = " "

-- Easier vertical navigation
vim.keymap.set('n', '<leader>d', '<C-d>zz', {})
vim.keymap.set('n', '<leader>u', '<C-u>zz', {})

-- Easier way to traverse back and forward through code
vim.keymap.set('n', '<leader>i', '<C-i>zz', {})
vim.keymap.set('n', '<leader>o', '<C-o>zz', {})

-- Save file
vim.keymap.set('n', '<leader>w', '<Cmd>:w<CR>', {})

-- Jump between methods
-- vim.keymap.set('n', '<leader>k', '<Cmd>[m<CR>', {})
-- vim.keymap.set('n', '<leader>j', '<Cmd>]m<CR>', {})

-- Jump between class declarations
-- vim.keymap.set('n', '<leader>n', '[[zz', {})
-- vim.keymap.set('n', '<leader>m', ']]zz', {})

-- Exit insert mode by inputting jj quickly
vim.keymap.set('i', 'jj', '<ESC>', {})
