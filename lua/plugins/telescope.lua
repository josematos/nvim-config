return {
    {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.6',
        dependencies = { 'nvim-lua/plenary.nvim' },
        config = function()
            local builtin = require("telescope.builtin")
            vim.keymap.set('n', '<leader>sf', builtin.find_files, {})
            vim.keymap.set('n', '<leader>sg', builtin.live_grep, {})
            vim.keymap.set('n', '<leader>sk', builtin.keymaps, {})
            vim.keymap.set('n', '<leader>ss', builtin.builtin, {})
            vim.keymap.set('n', '<leader>sw', builtin.grep_string, {})
            vim.keymap.set('n', '<leader>s.', builtin.oldfiles, {})
            vim.keymap.set('n', '<leader><leader>', builtin.buffers, {})
        end
    },
    {
        'nvim-telescope/telescope-ui-select.nvim',
        config = function()
            require("telescope").setup({
                find_files = {
                    theme = "dropdown",
                },
                extensions = {
                    ["ui-select"] = {
                        require("telescope.themes").get_dropdown({})
                    }
                }
            })
            require("telescope").load_extension("ui-select")
        end
    }
}
