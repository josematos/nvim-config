return {
    "romgrk/barbar.nvim",
    dependencies = {
        "lewis6991/gitsigns.nvim",
        "nvim-tree/nvim-web-devicons",
    },
    config = function()
        vim.g.barbar_auto_setup = false
        vim.keymap.set('n', '<leader>x', '<Cmd>BufferClose<CR>', {})
        vim.keymap.set('n', '<leader>h', '<Cmd>BufferPrevious<CR>', {})
        vim.keymap.set('n', '<leader>l', '<Cmd>BufferNext<CR>', {})
        vim.keymap.set('n', '<S-f>', '<Cmd>BufferPick<CR>', {})
    end,

    version = '^1.0.0'
}
